package com.example.demo.model.Core.SecurityGuidance;

import java.util.Collection;

public interface SecurityFilter<T> {
    Collection<T> filter(Collection<T> inputs);

    T filter(T input);
}
