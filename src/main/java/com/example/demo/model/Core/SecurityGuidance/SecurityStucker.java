package com.example.demo.model.Core.SecurityGuidance;

import java.util.Collection;

public interface SecurityStucker<T> {
    void stuck(Collection<T> inputs) throws OptionNotPermitedException;

    void stuck(T input) throws OptionNotPermitedException;
}
