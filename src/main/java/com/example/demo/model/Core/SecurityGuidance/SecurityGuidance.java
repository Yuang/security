package com.example.demo.model.Core.SecurityGuidance;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * 一个接口，用于实现rls(row level security),即数据权限控制
 */
public interface SecurityGuidance<T>{
    /**
     * @param inputs 从持久层读取出来的数据集合.
     * @return 经过过滤的数据集合.
     */
    Collection<T> readFilter(Collection<T> inputs);

    /**
     * @param input 从持久层读出来的Optional数据.
     *              这里不用Optional<T>而是直接用T，代表着应该手动先解包Optional再过滤。
     *              至于为什么，是idea发的警告，不应该将Optional用作参数.
     * @return 经过过滤的数据.
     */
    T readFilter(T input);
//    List<T> readFilter(List<T> input);

    /**
     * @param inputs 阻塞器，用于阻止用户未经授权地往数据库中插入数据.
     * @return 返回布尔值，如果有权插入该条数据则返回true，否则false.
     * 感觉更合适的方式是直接抛出无权访问异常,在代码上更简洁，但我不太确定怎么写比较好。
     */
    Boolean CreateStucker(Collection<T> inputs);

    /**
     * 同上一个方法，不过为单条数据准备的。这个接口推荐不做过多逻辑计算，而是封装成Collection后调用上一个方法.
     */
    Boolean CreateStucker(T input);

    Boolean UpdateStucker(Collection<T> inputs);

    Boolean UpdateStucker(T input);

    Boolean DeleteStucker(Collection<T> inputs);

    Boolean DeleteStucker(T input);
}
