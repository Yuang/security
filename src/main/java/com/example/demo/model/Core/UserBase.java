package com.example.demo.model.Core;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collection;

@Entity(name = "sys_user")
@Getter
@Setter
public class UserBase {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * 用户名
     */
    @Column
    private String name;

    public Collection<String> authorities(){
        return new ArrayList<>();
    }
}
