package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Id;

@javax.persistence.Entity
public class Entity {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
