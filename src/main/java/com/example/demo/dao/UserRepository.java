package com.example.demo.dao;

import com.example.demo.model.Core.UserBase;
import com.example.demo.model.Core.UserIdentity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserBase, Long> {
}
