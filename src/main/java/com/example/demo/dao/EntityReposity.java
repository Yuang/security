package com.example.demo.dao;

import com.example.demo.model.Entity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityReposity extends JpaRepository<Entity, Long> {
}
