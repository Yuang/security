package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.tags.Param;

import java.util.List;

@SpringBootApplication
@EnableWebSecurity
public class DemoApplication<T> {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
