package com.example.demo.Configuration.Security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests().anyRequest().authenticated()
                .and().formLogin().loginProcessingUrl("/login.html").permitAll()
                .failureForwardUrl("/login.html?login=error")
                .defaultSuccessUrl("/main",true)
                .and().logout().logoutUrl("/logout").logoutSuccessUrl("http://baidu.com")
                .and().csrf().disable();
    }

//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/master","/kaptcha");
//    }
}
